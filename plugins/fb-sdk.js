import {fbSdkConfig} from '@/assets/js/commons/constants'
window.fbAsyncInit = () => {
    FB.init(fbSdkConfig)
    window.dispatchEvent(new Event('fb-sdk-ready'))
}

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

export default ({ app }) => {
    /*
    ** Only run on client-side and only in production mode
    */
    //if (process.env.NODE_ENV !== 'production') return
    app.router.afterEach((to, from) => {
      
    })
  }