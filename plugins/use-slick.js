import Vue from 'vue'
import Slick from 'vue-slick'
import '@/node_modules/slick-carousel/slick/slick.css';
import '@/node_modules/slick-carousel/slick/slick-theme.scss';

Vue.use(Slick);