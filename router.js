import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/index'
import DealIndex from '@/pages/deal/index'
import BranchIndex from '@/pages/deal/branch'
import DealDetail from '@/pages/deal/detail'
import ProfileIndex from '@/pages/profile/index'
import ProfileDealDetail from '@/pages/profile/DetailDeal'
import ProfileDealEdit from '@/pages/profile/Update'
// landing
import FQA from '@/pages/landing/fqa'
import Policy from '@/pages/landing/policy'
import Partner from '@/pages/landing/partner'

Vue.use(Router)
export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        component: Index
      },
      {
        path: '/cau-hoi-thuong-gap',
        component: FQA
      },
      {
        path: '/quy-dinh-su-dung',
        component: Policy
      },
      {
        path: '/hop-tac-voi-bgift',
        component: Partner
      },
      {
        path: '/dia-diem',
        component: BranchIndex
      },
      {
        path: '/uu-dai',
        component: DealIndex
      },
      {
        path: '/uu-dai/:slug',
        component: DealDetail
      },
      {
        path: '/profile',
        component: ProfileIndex
      },
      {
        path: '/profile/chi-tiet-uu-dai',
        redirect: '/profile'
      },
      {
        path: '/profile/chi-tiet-uu-dai/:code',
        component: ProfileDealDetail
      },
      {
        path: '/profile/cap-nhat',
        component: ProfileDealEdit
      },
    ],
    scrollBehavior (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  })
}