const webpack = require('webpack')
module.exports = {
  /*
  ** Headers of the page
  */
  build: {
    vendor: [
      'axios',
      'babel-polyfill',
      'striptags'
    ]
  },
  plugins: [
    '@/plugins/global-mixins.js',
    '@/plugins/google-map.js',
    { src: '@/plugins/vuelidate.js', ssr: true },
    { src: '@/plugins/use-jquery', ssr: false },
    { src: '@/plugins/vue-notification.js', ssr: false },
    { src: '@/plugins/vue-lazyload.js', ssr: false },
    { src: '@/plugins/vue-cookie.js', ssr: false },
    { src: '@/plugins/click-outside.js', ssr: false },
    { src: '@/plugins/image-crop.js', ssr: false },
    { src: '@/plugins/datetimepicker.js', ssr: false },
    { src: '@/plugins/fb-sdk.js', ssr: false },
    { src: '@/plugins/ga.js', ssr: false },
    { src: '@/plugins/use-slick.js', ssr: false },
    { src: '@/plugins/use-vue-bar.js', ssr: false }
  ],
  modules: [
    '@nuxtjs/router',
    '@nuxtjs/font-awesome',
    'cookie-universal-nuxt'
  ],
  router: {
    middleware: [
      'fbsdk-parse',
      'check-auth',
      'preload-data'
    ]
  },
  generate: {
    routes: [
      '/'
    ]
  },
  head: {
    htmlAttrs: {
      lang: 'vi-VN',
    },
    title: 'Bgift - Ưu đãi làm đẹp tốt nhất',
    meta: [
      { charset: 'utf-8' },
      { name: 'language', content: 'vi-VN' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: "description", name: 'description', content: 'Bgift - Ưu đãi làm đẹp tốt nhất' },
      { name: 'geo.region', content: 'VN' },
      { name: 'geo.placename', content: 'Da Nang City' },
      { name: 'geo.position', content: '16.0472484; 108.1716864' },
      { name: 'ICBM', content: '16.0472484, 108.1716864' },
      //facebook open graph
      { property: "og:type", content: "article" },
      { hid: "og:title", property: "og:title", content: "Bgift - Ưu đãi làm đẹp tốt nhất" },
      { hid: "og:url", property: "og:url", content: "https://bgift.vn" },
      { hid: "og:description", property: "og:description", content: "Bgift - Ưu đãi làm đẹp tốt nhất" },
      { hid: "og:image", property: "og:image", content: "" },
      { property: "article:publisher", content: "https://www.facebook.com/BGiftApp/" },
      { property: "article:author", content: "https://www.facebook.com/BGiftApp/" },
      //dublink core
      { name: "DC.Title", content: "Bgift - Ưu đãi làm đẹp tốt nhất" },
      { name: "DC.Creator", content: "Bgift Team" },
      { name: "DC.Subject", content: "Bgift" },
      { name: "DC.Description", content: "Bgift - Ưu đãi làm đẹp tốt nhất" },
      { name: "DC.Publisher", content: "Bgift" },
      { name: "DC.Contributor", content: "Bgift Team" },
      { name: "DC.Date", content: "2018-03" },
      { name: "DC.Type", content: "text" },
      { name: "DC.Source", content: "Bgift.vn" },
      { name: "DC.Language", content: "vi" },
      { name: "DC.Coverage", content: "vietnam" },
      { name: "twitter:card", content: "summary_large_image" },
      //twitter card
      { name: "twitter:site", content: "@Bgift" },
      { name: "twitter:creator", content: "@Bgift" },
      { hid: "twitter:title", name: "twitter:title", content: "Chính sách sử dụng" },
      { hid: "twitter:description", name: "twitter:description", content: "" },
      { hid: "twitter:image", name: "twitter:image", content: "" },
      //facebook admin
      // { property: "fb:pages", content: "1700708280179463" },
      // { property: "fb:app_id", content: "742854255855497" },
      // { property: "fb:admins", content: "100004488484051" },

    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/img/icon.png' },
      { href:"https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=vietnamese", rel:"stylesheet"}
    ],
    script: [
      {src: 'https://sdk.accountkit.com/vi_VN/sdk.js'}
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/node_modules/bootstrap/dist/css/bootstrap.css',
    '@/assets/css/components.scss',
    '@/assets/css/style.scss'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#FFFFFF' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient, isServer }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (isServer) {
        config.externals = [
          require('webpack-node-externals')({
            whitelist: [/^vue-slick/]
          })
        ]
      }
      if (!isClient) {
        // This instructs Webpack to include `vue2-google-maps`'s Vue files
        // for server-side rendering
        config.externals.splice(0, 0, function (context, request, callback) {
          if (/^vue2-google-maps($|\/)/.test(request)) {
            callback(null, false)
          } else {
            callback()
          }
        })
      }
    }
  }
}
