import groupTextApi from '@/assets/js/api/grouptext'
import {SITE_URL} from '@/assets/js/commons/constants'
export default function (context) {
    const {route, store} = context
    return Promise.all([groupTextApi.getGroupTextBySlug('','seo-homepage'),
                        groupTextApi.getGroupTextBySlug('','noi-dung-footer'),
                        ])
                        .then(values=> {
      var seoHome = values[0].data.groupText
      var footerText = values[1].data.groupText
      var striptags = require('striptags')
      store.commit("SET_SEO_HOMEPAGE", {
        seoTitle: seoHome.title,
        seoKeyword: seoHome.subTitle,
        seoDescription: striptags(seoHome.description),
        seoUrl: `${SITE_URL}${route.fullPath}`
      })
      store.commit("SET_TEXT_FOOTER", footerText.description)
    })
}
