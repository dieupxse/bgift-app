import {CLIENT_AUTH_TOKEN, CLIENT_AUTH_refreshToken} from '@/assets/js/commons/constants'
import authApi from '@/assets/js/api/client-authen'

export default function ({store, route, redirect, app}) {
    var authToken = app.$cookies.get(CLIENT_AUTH_TOKEN)
    var RefreshAuthToken = app.$cookies.get(CLIENT_AUTH_refreshToken)
    var dateTime = new Date()
    var setDateExpire = (date) => {
      dateTime.setDate(dateTime.getDate() + date)
      return dateTime
    }
    if(authToken) {
        return authApi.getCurrentUser(authToken).then((rs)=> {
            store.commit('SET_AUTH',rs.data)
        }).catch((err)=> {
            app.$cookies.remove(CLIENT_AUTH_TOKEN)
            store.commit('REMOVE_AUTH')
        })
    }
    else{
      if(RefreshAuthToken){
        return authApi.login('', {
          refreshToken: RefreshAuthToken,
          grantType: "refreshToken"
        }).then(rs => {
          app.$cookies.remove(CLIENT_AUTH_TOKEN)
          app.$cookies.remove(CLIENT_AUTH_refreshToken)
          app.$cookies.set(CLIENT_AUTH_TOKEN,rs.data.token, {expires: setDateExpire(rs.data.expTokenDate)})
          app.$cookies.set(CLIENT_AUTH_refreshToken,rs.data.refreshToken, {expires: setDateExpire(rs.data.expRefreshTokenDate)})

          return authApi.getCurrentUser(rs.data.token).then((us)=> {
            store.commit('SET_AUTH',us.data)
          }).catch((err)=> {
          })
        }).catch((err)=> {
            app.$cookies.remove(CLIENT_AUTH_refreshToken)
            store.commit('REMOVE_AUTH')
        })
      }
    }
}
