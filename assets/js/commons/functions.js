import moment from 'moment'
const formatDate = (date,format) => {
    return moment(date).format(format);
}
const FormatThousand = (value, unit = "đ") => {
  return `${(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'))} ${unit}`
}
const getStatusLabel = (status) => {
  switch(status) {
      case 1:
          return "Đang chờ xác nhận"
      case 2:
          return "Đã xác nhận"
      case 3:
          return "Đã hoàn tất"
      case 4:
          return "Đã hủy"
      default:
          return ""
  }
}
const getLink = (slug, type) => {
    switch(type) {
        case 'custom-link':
            return `/${slug}`
        case 'category':
            return genBlogCategoryPath(slug)
        case 'page':
            return genBlogPagePath(slug)
        default:
            return '/'
    }
}
const stripHtml = (html) =>
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}
const formatDeal = (value) => {
  var str =  (value+'').substring(0, (value+'').length - 3)
  return str
}
export default {
    formatDate,
    FormatThousand,
    getLink,
    stripHtml,
    formatDeal
}
