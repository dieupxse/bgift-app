const API_BASE_URL = 'https://api.bgift.vn'
const API_SMS_URL = 'https://api.ctnet.vn'
const API_AUTHEN_URL = 'https://api.glamme.vn'
const API_MEDIA_URL = 'https://api.glamme.vn'
// const API_BASE_URL = 'http://localhost:50608'
const AUTH_TOKEN_KEY = ''
const CLIENT_AUTH_TOKEN = "XD_TT_DM"
const CLIENT_AUTH_refreshToken = "XS_TT_DM"
const GOOGLE_API_KEY = "AIzaSyASObA56AeVvyAi-uVlC8mOtFZEDzbojzk"
const USERNAME_SMS = "bgif"
const PASSWORD_SMS = "ctnet@@1812"
const SITE_URL = 'https://bgift.vn'
const DISCOUNT_VALUE = {
  percent: 1,
  money: 2,
  sameprice: 3
}
const fbSdkConfig = {
  appId: '606205936423055',
  cookie: true,
  xfbml: true,
  autoLogAppEvents : true,
  version: 'v2.12'

}
const ACCOUNT_KIT = {
  version: 'v1.1',
  redirectUrl: 'http://localhost:3005/',
  secret: 'e57348f4e00aa184f663876c1d1fdf12',
}
export {
  USERNAME_SMS,
  PASSWORD_SMS,
  API_BASE_URL,
  API_SMS_URL,
  AUTH_TOKEN_KEY,
  CLIENT_AUTH_TOKEN,
  CLIENT_AUTH_refreshToken,
  GOOGLE_API_KEY,
  DISCOUNT_VALUE,
  API_AUTHEN_URL,
  fbSdkConfig,
  SITE_URL,
  API_MEDIA_URL,
  ACCOUNT_KIT
}

