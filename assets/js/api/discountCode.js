import {http} from './helper'
export default {
  createCode (token, data){
    return new Promise((resolve, reject) => {
      http(token).post(`/product/api/DiscountCode`, data).then( rs=> {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getDetailCodeByCode (token, code){
    return new Promise((resolve, reject) => {
      http(token).get(`/product/api/DiscountCode/GetDiscountCodeByCurrentUserAndCode?code=${code}`).then( rs=> {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getListDetailCode (token, params) {
    return new Promise((resolve, reject) => {
      http(token).get(`/product/api/DiscountCode/GetListDiscountCodeByCurrentUser`, {params: params}).then( rs=> {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}