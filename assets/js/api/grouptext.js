import {http} from './helper'
export default {
  getGroupText(token, keyword="", page=1, rowPerPage=20, orderby="CreateDate", order="desc") {
    return new Promise((resolve, reject) => {
      http(token).get(`/config/api/grouptext`, {
            params: {
                keyword,
                page,
                rowPerPage,
                orderby,
                order
            }
        }).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getGroupTextBySlug(token, slug) {
    return new Promise((resolve, reject) => {
      http(token).get(`/config/api/grouptext/slug/${slug}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getGroupTextByGroupId(token, id) {
    return new Promise((resolve, reject) => {
      http(token).get(`/config/api/grouptext/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  createGroupText(token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`/config/api/grouptext`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  updateGroupText(token,id,data) {
    return new Promise((resolve, reject) => {
      http(token).put(`/config/api/grouptext/${id}`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  },
  deleteGroupText(token,id) {
    return new Promise((resolve, reject) => {
      http(token).delete(`/config/api/grouptext/${id}`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
      })
  }
}