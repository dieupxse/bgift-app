import {http} from './helper'

export default {
  getAllCategory (params) {
    return new Promise((resolve, reject) => {
      http('').get(`/product/api/category/`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}