import {http} from './helper'
import {API_AUTHEN_URL} from '@/assets/js/commons/constants'
export default {
  loginFacebook(token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`${API_AUTHEN_URL}/account/api/account/AuthenByFacebook`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  login(token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`${API_AUTHEN_URL}/account/api/account/AuthenByEmailPass`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  register(token, data) {
    return new Promise((resolve, reject) => {
      http(token).post(`${API_AUTHEN_URL}/account/api/account/register`, data).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  getCurrentUser (token) {
    return new Promise((resolve, reject) => {
      http(token).get(`${API_AUTHEN_URL}/account/api/account/GetCurrentAccount`).then(response => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  updateCurrentUser(token, auth) {
    return new Promise((resolve, reject) => {
      http(token).put(`${API_AUTHEN_URL}/account/api/account`, auth).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
  updateAccountKit (token, auth) {
    return new Promise((resolve, reject) => {
      http(token).put(`${API_AUTHEN_URL}/account/api/account/accountkit`, auth).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
