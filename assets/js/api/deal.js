import {http} from './helper'

export default {
  getAllCampaign (params) {
    return new Promise((resolve, reject) => {
      http().get(`/product/api/CampaignDetail`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getCampaignDetailBySlug (slug) {
    return new Promise((resolve, reject) => {
      http().get(`/product/api/CampaignDetail/GetCampaignDetailBySlug?slug=${slug}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getSameBranchs (slug) {
    return new Promise((resolve, reject) => {
      http().get(`/product/api/CampaignDetail/GetSameBranchs?slug=${slug}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
}