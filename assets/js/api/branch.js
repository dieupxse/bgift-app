import {http} from './helper'

export default {
  getAllProviders (params) {
    return new Promise ((resolve, reject) => {
      // http('').get(`http://localhost:50608/api/branch/`, {params: params}).then(rs => {
      http('').get(`/product/api/branch/`, {params: params}).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
  getProviderById  (id){
    return new Promise ((resolve, reject) => {
      http('').get(`/product/api/branch/getbranchbyid/${id}`).then(rs => {
        resolve(rs.data)
      }).catch(err => {
        reject(err.response)
      })
    })
  },
}