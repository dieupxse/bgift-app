import axios from 'axios'
import {USERNAME_SMS,PASSWORD_SMS,API_SMS_URL} from '@/assets/js/commons/constants'

const httpSMS = (token) =>  {
  return axios.create({
    headers: {
      Authorization: `Bearer ${token}`,
      "content-type": "application/json",
    }
  })
}
export default {
  sendSMS (dataSMS) {
    return httpSMS().post(`${API_SMS_URL}/dang-nhap`,{"username": USERNAME_SMS,"password": PASSWORD_SMS}).then(rs => {
      //resolve(rs.data)
      if(rs.data.status==0){
        return new Promise((resolve, reject) => {
          httpSMS(rs.data.data.accessToken).post(`${API_SMS_URL}/gui-tin`,dataSMS).then(dt => {
            resolve(dt.data)
          }).catch(err => {
              reject(err.response)
          })
        })
      }
    })
  }
}
