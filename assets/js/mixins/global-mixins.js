// jshint ignore: start
import {mapGetters} from 'vuex'
export default {
  notifications: {
    showNotification: { // You can have any name you want instead of 'showLoginError'
      title: '',
      message: '',
      type: '', // You also can use 'VueNotifications.types.error' instead of 'error'
      timeout: 3000
    }
  },
  computed: {
    ...mapGetters({
      seoHome: 'GET_SEO_HOMEPAGE',
      $mobile: 'GET_MOBILE',
      auth: 'GET_AUTH',
      isAccountKitInit:  'GET_STATUS_ACCOUNT_KIT'
    })
  }
}
