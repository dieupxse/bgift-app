import {CLIENT_AUTH_TOKEN, CLIENT_AUTH_refreshToken} from '@/assets/js/commons/constants'
export default {
  computed: {
    $authToken () {
      return this.$cookies.get(CLIENT_AUTH_TOKEN)
    }
  },
  methods: {
    $removeAuthToken() {
      this.$cookies.remove(CLIENT_AUTH_TOKEN)
      this.$cookies.remove(CLIENT_AUTH_refreshToken)
    },
    $setAuthToken(token,options) {
      this.$cookies.set(CLIENT_AUTH_TOKEN,token, options)
    },
    $setAuthRefreshToken(RfToken, options) {
      this.$cookies.set(CLIENT_AUTH_refreshToken, RfToken, options)
    },
    $setCookie(key,value,options = {}) {
      this.$cookies.set(key,value,options)
    },
    $getCookie(key) {
      return this.$cookies.get(key)
    },
    closeLoginModal() {
      document.body.classList.remove('modal-body-custom');
      this.$store.commit('TOGGLE_LOGIN_MODAL',false)
    },
    showLoginModal() {
      document.body.classList.add('modal-body-custom');
      this.$store.commit('TOGGLE_LOGIN_MODAL',true)
    }
  }
}
