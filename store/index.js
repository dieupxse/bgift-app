import Vuex from 'vuex'
//module store
import provider from './provider'
import deal from './hotdeal'
import moment from 'moment'

const createStore = () => {
  return new Vuex.Store({
    state: {
      authUser: null,
      showLoginModal: false,
      seoHomePage: {},
      footer: {},
      isAccountKitInit: false
    },
    mutations: {
      TOGGLE_LOGIN_MODAL(state, val) {
        state.showLoginModal = val
      },
      SET_AUTH(state, user) {
        state.authUser = user
      },
      REMOVE_AUTH(state) {
        state.authUser = null
      },
      SET_SEO_HOMEPAGE(state, seo) {
        state.seoHomePage = seo
      },
      SET_TEXT_FOOTER(state, seo) {
        state.footer = seo
      },
      SET_STATUS_ACCOUNT_KIT(state, status) {
        state.isAccountKitInit = status
      },
    },
    actions: {
      async nuxtServerInit ({ commit, dispatch }, { req }) {
        var paramsHotDeal = {
          status: -1,
          categoryId: -1,
          campaignId: -1,
          productId: -1,
          campaignType: -1,
          campainSortType: 1,
          isActive: 1,
          keyword: '',
          fromDate: '', // moment().format('DD/MM/YYYY HH:mm:ss'),
          toDate: '', //moment().format('DD/MM/YYYY HH:mm:ss'),
          page: 1,
          rowPerPage: 6,
          orderBy: 'createdDate',
          order: 'desc'
        }
        await dispatch('actionGetListHotDeals', paramsHotDeal)
      }
    },
    getters: {
      IS_SHOW_LOGIN_MODAL (state) {
        return state.showLoginModal
      },
      GET_AUTH(state) {
        return state.authUser
      },
      GET_SEO_HOMEPAGE(state) {
        return state.seoHomePage
      },
      GET_TEXT_FOOTER(state) {
        return state.footer
      },
      GET_STATUS_ACCOUNT_KIT(state){
        return state.isAccountKitInit
      }
    },
    modules: {
      provider,
      deal
    }
  })
}
export default createStore
