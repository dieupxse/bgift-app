import dealApi from '@/assets/js/api/deal'

const state = () => ({
  listHotDeals: [],
  paramsHotDeal: {
    
  }
})

const actions = {
  async actionGetListHotDeals ({commit}, params) {
    commit('setHotDealSearchParams', params)
    var result = await dealApi.getAllCampaign(params)
    commit('setHotDeals', result)
  }
}

const mutations = {
  setHotDeals (state, data) {
    state.listHotDeals = data
  },
  setHotDealSearchParams (state, params) {
    state.paramsHotDeal = params
  }
}

const getters = {
  getListHotDeals (state) {
    return state.listHotDeals
  },
  getHotDealSearchParam (state) {
    return state.paramsHotDeal
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}  