import branchApi from '@/assets/js/api/branch'

const state = () => ({
  listBranchs: [],
  paramsBranchs: {
    parentId: -1,
    categoryId: -1,
    page: 1,
    rowPerPage: 8,
    orderBy: 'createdDate', 
    order: 'desc'
  }
})

const actions = {
  async actionGetListProviders ({commit}, params) {
    commit('setSearchParams', params)
    var result = await branchApi.getAllProviders(params)
    commit('setListProviders', result)
  }
}

const mutations = {
  setListProviders (state, data) {
    state.listBranchs = data
  },
  setSearchParams (state, params) {
    state.paramsBranchs = params
  }
}

const getters = {
  getListProviders (state) {
    return state.listBranchs
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}  